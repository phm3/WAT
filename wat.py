#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import time
import sys
import datetime
import os
import select
import ntplib
from time import ctime
import inquirer
import json

watches_data = []
with open('watches.dat') as f:
    for line in f:
        watches_data.append(json.loads(line))


questions = [inquirer.List(
    'watchlist', message="Select the watch from the list:", choices=watches_data[0], ), ]
answers = inquirer.prompt(questions)


if answers["watchlist"] == "NEW":

        # get the name of the watch
    watch_name = raw_input("Enter a name of the watch: ")

    new_one = "%s" % watch_name
    with open('watches.dat') as f:
        content = json.load(f)
        content.append(new_one)

    with open('watches.dat', 'w') as f:
        json.dump(content, f)


else:

    watch_name = answers["watchlist"]
# this most likely it is used in windows, on Mac OS is not required
try:
    c = ntplib.NTPClient()
    response = c.request('pool.ntp.org')
    print(ctime(response.tx_time))
    print("Synchronized with atomic.")
    date_object = datetime.datetime.strptime(time.ctime(), "%a %b %d %H:%M:%S %Y")
    #print (date_object.hour,":",date_object.minute)
    datetime.datetime.strptime(time.ctime(), "%a %b %d %H:%M:%S %Y")
    watch_time = "%s:%s:00" % (date_object.hour,date_object.minute+1)

except:
    watch_time = "%s:%s:00" % (datetime.datetime.now().hour,datetime.datetime.now().minute)
  

while True:


    os.system('cls' if os.name == 'nt' else 'clear')
    #print (watch_time.hour)
    if 'date_object' in locals():
        date_object = datetime.datetime.strptime(time.ctime(), "%a %b %d %H:%M:%S %Y")
        watch_time = "%s:%s:00" % (date_object.hour,date_object.minute+1)
        print("Press Enter when your watch shows: %s:%s:00" % (date_object.hour, date_object.minute+1))
    else:
        print("Press Enter when your watch shows: %s:%s:00"  % (datetime.datetime.now().hour,datetime.datetime.now().minute))

    now = datetime.datetime.now()
    print(now.strftime("%H:%M:%S"), end="\r")

    sys.stdout.flush()
    time.sleep(.1)
    if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        time = now.strftime("%H:%M:%S")
        line = raw_input()
        print("Time registered for %s > %s" % (watch_name, time))
        os.system("echo '%s;%s;%s;%s' >> %s.csv" %
                  (watch_name, time, watch_time, now, watch_name))
        break
        os.exit()
